const { factorial } = require('../src/util.js');

const { expect, assert } = require('chai');
//gets the expect and assert functions from chai to be used

//accepts two parameters - a string explaining what the test should do, and a callback function which contains the 'actual test'

//ASSERT: Fails fast, aborting the current function. EXPECT: Continues after the failure.


describe('test_fun_factorials', ()=>{
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	})

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1);
	})

	//negative num
	it('test_fun_factorials_neg1_is_undefined', () => {
		const product = factorial(-1);
		expect(product).to.equal(undefined);
	})

	//other types
	it("test_fun_factorials_invalid_number_is_undefined", () => {
		const product = factorial("25");
		expect(product).to.equal(undefined);
	})

})
